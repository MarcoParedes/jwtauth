﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Domain.Interfaces;
using WebApplication.Domain.Request;
using WebApplication.Exceptions;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        private readonly IProductosService productosService;
        public ProductosController(IProductosService productosService)
        {
            this.productosService = productosService;
        }

        [HttpGet, Route("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = (typeof(List<Producto>)))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(List<ComponentErrorDetail>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Get([FromQuery] int categoryId)
        {
            List<Producto> productos = productosService.GetAll(categoryId);
            return Ok(productos);
        }

        [HttpGet, Route("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = (typeof(Producto)))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(List<ComponentErrorDetail>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetById(int id)
        {
            Producto producto = productosService.GetById(id);
            return Ok(producto);
        }

        [HttpPost, Route("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = (typeof(Producto)))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(List<ComponentErrorDetail>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Create([FromBody] ProductoRequest request)
        {
            Producto producto = productosService.Create(request);
            return Created($"api/v1/productos/{producto.Id}", producto);
        }

        [HttpPut, Route("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = (typeof(Producto)))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(List<ComponentErrorDetail>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Update(int id, [FromBody] ProductoRequest request)
        {
            Producto producto = productosService.Update(id, request);
            return Ok(producto);
        }

        [HttpDelete, Route("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = (typeof(Producto)))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(List<ComponentErrorDetail>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            Producto producto = productosService.Remove(id);
            return Ok(producto);

        }

    }
}