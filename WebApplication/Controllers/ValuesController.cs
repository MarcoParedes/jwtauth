﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = (typeof(List<string>)))]
        public IActionResult Get()
        {
            return Ok(new List<string>() { "value 1", "value 2", "value 3" });
        }

        // GET api/values/5
        [Authorize]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = (typeof(string)))]
        public IActionResult Get(int id)
        {
            return Ok($"value {id}");
        }

    }
}
