﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Domain.Interfaces;
using WebApplication.Exceptions;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SeguridadController : ControllerBase
    {
        private readonly ISeguridadService seguridadService;
        public SeguridadController(ISeguridadService seguridadService)
        {
            this.seguridadService = seguridadService;
        }

        [HttpPost]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Token))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(List<ComponentErrorDetail>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Login([FromBody] LoginRequest request)
        {
            Usuario usuario = GetUser(request.Email, request.Password);
            if (usuario == null) return NotFound();

            DateTime date = DateTime.UtcNow;
            TimeSpan expireDate = TimeSpan.FromHours(5);
            Token token = seguridadService.GenerateToken(date, usuario, expireDate);

            return Ok(token);
        }

        private Usuario GetUser(string email, string password)
        {
            var usuarios = new List<Usuario>
            {
                new Usuario { Id = 1, Nombre = "test1", Email = "test1@gmail.com", Password = "123" },
                new Usuario { Id = 2, Nombre = "test2", Email = "test2@gmail.com", Password = "1234" },
                new Usuario { Id = 3, Nombre = "test3", Email = "test3@gmail.com", Password = "12345" },
                new Usuario { Id = 4, Nombre = "test4", Email = "test4@gmail.com", Password = "123456" }
            };

            return usuarios.FirstOrDefault(user => user.Email.Equals(email) && user.Password.Equals(password));
        }

    }

    public class LoginRequest
    {
        [Required]
        [MinLength(8)]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }

}