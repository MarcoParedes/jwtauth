﻿
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Domain.Request
{
    public class ProductoRequest
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(5)]
        public string Nombre { get; set; }

        [Required]
        public int CategoriaId { get; set; }

    }
}
