﻿using System;
using WebApplication.Models;

namespace WebApplication.Domain.Interfaces
{
    public interface ISeguridadService
    {
        Token GenerateToken(DateTime date, Usuario user, TimeSpan validDate);
    }
}
