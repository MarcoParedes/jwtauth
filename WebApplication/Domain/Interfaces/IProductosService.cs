﻿using System.Collections.Generic;
using WebApplication.Domain.Request;
using WebApplication.Models;

namespace WebApplication.Domain.Interfaces
{
    public interface IProductosService
    {
        List<Producto> GetAll(int categoryId);
        Producto GetById(int id);
        Producto Update(int id, ProductoRequest request);
        Producto Remove(int id);
        Producto Create(ProductoRequest request);

    }
}
