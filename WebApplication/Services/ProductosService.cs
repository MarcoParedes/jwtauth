﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication.Domain.Interfaces;
using WebApplication.Domain.Request;
using WebApplication.Models;
using static WebApplication.Exceptions.BasicExceptions;

namespace WebApplication.Services
{
    public class ProductosService : IProductosService
    {
        public Producto Create(ProductoRequest request)
        {
            Producto producto = new Producto()
            {
                Id = request.Id,
                Nombre = request.Nombre,
                CategoriaId = request.CategoriaId
            };
            GetProductos().Add(producto);
            return producto;
        }

        public List<Producto> GetAll(int categoryId)
        {
            List<Producto> productos = GetProductos()
                                        .Where(product => product.CategoriaId == categoryId)
                                        .ToList();

            return productos;
        }

        public Producto GetById(int id)
        {
            Producto producto = getProduct(id);
            return producto;
        }

        public Producto Remove(int id)
        {
            Producto producto = getProduct(id);
            GetProductos().Remove(producto);
            return producto;
        }

        public Producto Update(int id, ProductoRequest request)
        {
            Producto producto = getProduct(id);
            producto.Nombre = request.Nombre;
            producto.CategoriaId = request.CategoriaId;
            return producto;
        }

        private Producto getProduct(int id)
        {
            Producto producto = GetProductos().Find(product => product.Id == id);
            if (producto == null) throw new NotFoundException();
            return producto;
        }

        private static List<Producto> GetProductos()
        {
            return new List<Producto>() {
                new Producto() {Id = 1, Nombre = "producto 1", CategoriaId = 1 },
                new Producto() {Id = 2, Nombre = "producto 2", CategoriaId = 1 },
                new Producto() {Id = 3, Nombre = "producto 3", CategoriaId = 2 },
                new Producto() {Id = 4, Nombre = "producto 4", CategoriaId = 2 },
                new Producto() {Id = 5, Nombre = "producto 5", CategoriaId = 3 }
            };
        }

    }
}
