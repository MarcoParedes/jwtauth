﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApplication.Domain.Interfaces;
using WebApplication.Models;

namespace WebApplication.Services
{
    public class SeguridadService : ISeguridadService
    {
        private IConfiguration Configuration;

        public SeguridadService(IConfiguration config)
        {
            Configuration = config;
        }

        public Token GenerateToken(DateTime date, Usuario user, TimeSpan validDate)
        {
            DateTime expire = date.Add(validDate);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(
                    JwtRegisteredClaimNames.Iat,
                    new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds().ToString(),
                    ClaimValueTypes.Integer64
                ),
                new Claim(ClaimTypes.Name, user.Nombre),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                //roles
                new Claim(ClaimTypes.Role, "Admin")
            };

            var signingCredentials = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["AuthenticationSettings:SigningKey"])),
                SecurityAlgorithms.HmacSha256Signature
            );

            var jwt = new JwtSecurityToken(
            issuer: Configuration["AuthenticationSettings:Issuer"],
            audience: Configuration["AuthenticationSettings:Audience"],
            claims: claims,
            notBefore: date,
            expires: expire,
            signingCredentials: signingCredentials
            );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return new Token()
            {
                AccessToken = encodedJwt,
                ExpiresAt = expire,
                UserName = jwt.Payload.Sub,
                TokenType = jwt.Header.Typ,
                ExpiresIn = date
            };
        }

    }
}
