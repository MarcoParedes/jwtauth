﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace WebApplication.Exceptions
{
    public static class ComponentErrorExtensions
    {
        public static ComponentError FromModelState(this ComponentError ce, ModelStateDictionary modelState)
        {
            foreach (var item in modelState)
            {
                foreach (var error in item.Value.Errors)
                {
                    ce.AddModelError(item.Key, error.Exception, errorCode: null, message: error.ErrorMessage);
                }
            }

            return ce;
        }
    }
}
