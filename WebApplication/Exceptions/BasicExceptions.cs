﻿using System;
using System.Collections.Generic;

namespace WebApplication.Exceptions
{
    public class BasicExceptions
    {
        public class ConflictException : ApplicationException
        {
            public ConflictException() : base()
            {
            }
        }

        public class NotFoundException : ApplicationException
        {
            public NotFoundException() : base()
            {
            }
        }

        public class BadRequestException : ApplicationException
        {
            public List<ComponentErrorDetail> Errors { get; private set; }

            public BadRequestException() : base()
            {
            }

            public BadRequestException(List<ComponentErrorDetail> errors) : base()
            {
                Errors = errors;
            }
        }

        public class ForbiddenException : ApplicationException
        {
            public ForbiddenException() : base()
            {
            }
        }

        public class InternalServerErrorException : ApplicationException
        {
            public InternalServerErrorException(string message) : base(message)
            {
            }
        }

        public class UnauthorizedException : ApplicationException
        {
            public UnauthorizedException(string message) : base(message)
            {
            }
        }

        public class PreconditionFailedException : ApplicationException
        {
            public PreconditionFailedException(string message) : base(message)
            {
            }
        }

    }
}
