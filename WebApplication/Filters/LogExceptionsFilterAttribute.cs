﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using static WebApplication.Exceptions.BasicExceptions;

namespace WebApplication.Filters
{
    public static class LogExceptionsFilterAttribute
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        if (contextFeature.Error is NotFoundException)
                        {
                            var exception = (NotFoundException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(new { }));
                            return;
                        }

                        if (contextFeature.Error is BadRequestException)
                        {
                            var exception = (BadRequestException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(exception.Errors));
                            return;
                        }

                        if (contextFeature.Error is ConflictException)
                        {
                            var exception = (ConflictException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.Conflict;
                            await context.Response.WriteAsync(exception.Message);
                            return;
                        }

                        if (contextFeature.Error is PreconditionFailedException)
                        {
                            var exception = (PreconditionFailedException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.PreconditionFailed;
                            await context.Response.WriteAsync(exception.Message);
                            return;
                        }

                        if (contextFeature.Error is ForbiddenException)
                        {
                            var exception = (ForbiddenException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(new { }));
                            return;
                        }

                        if (contextFeature.Error is UnauthorizedException)
                        {
                            var exception = (UnauthorizedException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            await context.Response.WriteAsync(exception.Message);
                            return;
                        }

                        if (contextFeature.Error is Newtonsoft.Json.JsonSerializationException)
                        {
                            var exception = (Newtonsoft.Json.JsonSerializationException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.UnprocessableEntity;
                            await context.Response.WriteAsync(exception.Message);
                            return;
                        }

                        if (contextFeature.Error is FormatException)
                        {
                            var exception = (FormatException)contextFeature.Error;
                            context.Response.StatusCode = (int)HttpStatusCode.UnprocessableEntity;
                            await context.Response.WriteAsync(exception.Message);
                            return;
                        }

                        ServerErrorModel serverError = new ServerErrorModel(contextFeature.Error);
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(serverError));

                        //logger.LogError($"Something went wrong: {contextFeature.Error}");

                    }
                });
            });
        }

    }

    public class ServerErrorModel
    {
        public Exception InnerException { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public ServerErrorModel(Exception exception)
        {
            InnerException = exception.InnerException;
            Message = exception.Message;
            StackTrace = exception.StackTrace;
        }
    }

}
